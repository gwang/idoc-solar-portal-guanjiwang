 /*******************************************************************************
 * Copyright 2010-2014 CNES - CENTRE NATIONAL d'ETUDES SPATIALES
 *
 * This file is part of SITools2.
 *
 * SITools2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SITools2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SITools2.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.cnes.sitools.resources.sdo.order;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;

import org.restlet.data.Reference;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.resource.ResourceException;

import fr.cnes.sitools.common.exception.SitoolsException;
import fr.cnes.sitools.dataset.database.DatabaseRequest;
import fr.cnes.sitools.datasource.jdbc.model.AttributeValue;
import fr.cnes.sitools.datasource.jdbc.model.Record;
import fr.cnes.sitools.plugins.resources.model.ResourceModel;
import fr.cnes.sitools.plugins.resources.model.ResourceParameter;
import fr.cnes.sitools.resources.order.AbstractDatasetOrderResource;
import fr.cnes.sitools.resources.order.utils.ListReferencesAPI;
import fr.cnes.sitools.resources.order.utils.OrderAPI;
import fr.cnes.sitools.resources.order.utils.OrderResourceUtils;
import fr.cnes.sitools.server.Consts;
import fr.cnes.sitools.util.RIAPUtils;

/**
 * Default OrderResource implementation.
 * <p>
 * The parameter colUrl is used to specify a column containing the list of URLs of the files to order. Each file is then
 * either copied or Zipped to the userstorage of the user.
 * </p>
 * 
 * 
 * @author last author: guanji
 */
public class SDOOrderResource extends AbstractDatasetOrderResource {
  /** Maximum number of file to download authorized, default to -1 => no limit */
  private int nbMaxDownload = -1;

  @Override
  public ListReferencesAPI listFilesToOrder(DatabaseRequest dbRequest) throws SitoolsException {
    task.setCustomStatus("Creating list of files to order");
    ResourceModel resourceModel = getModel();
    ResourceParameter nbMaxDownloadParam = resourceModel.getParameterByName("too_many_selected_threshold");
    if (nbMaxDownloadParam != null && !"".equals(nbMaxDownloadParam)) {
      try {
        nbMaxDownload = Integer.parseInt(nbMaxDownloadParam.getValue());
      }
      catch (NumberFormatException e) {
        nbMaxDownload = -1;
      }
    }
    
    if (nbMaxDownload != -1 && nbMaxDownload < dbRequest.getCount()) {
      ResourceParameter errorTextParam = resourceModel.getParameterByName("too_many_selected_threshold_text");
      String errorText = (errorTextParam != null && !"".equals(errorTextParam.getValue())) ? errorTextParam.getValue()
          : "Too many file selected";
      throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, errorText);
    }

    ResourceParameter colUrl = resourceModel.getParameterByName("colUrl");
    if (colUrl.getValue() == null || colUrl.getValue().equals("")) {
      throw new SitoolsException("NO COLUMN DEFINED FOR ORDER");
    }

    ListReferencesAPI list = new ListReferencesAPI(settings.getPublicHostDomain() + settings.getString(Consts.APP_URL));
    while (dbRequest.nextResult()) {
      /** Get each line of record from dbRequest*/
      Record rec = dbRequest.getRecord();
      AttributeValue attributeValue = OrderResourceUtils.getInParam(colUrl, rec); 
      
      if (attributeValue != null && attributeValue.getValue() != null) {
        
        /** Create an instance for SdoMetaModel
         **  
         *  */
        System.out.println("the attribute is not null, it equals : " + attributeValue.getValue().toString());
        SdoMetaModel sdometamodel = new SdoMetaModel();
        sdometamodel = getSdoModel(rec);
        
        String filePath = (String) attributeValue.getValue(); 
        /** http://localhost:8182/sitools/datastorage/user/2055462/hmi.sharp_cea_720s_nrt/SUM12/D817230213/S00000 for hmi
         *  http://localhost:8182/sitools/datastorage/user/SUM12/D817230213/S00000 for aia
         */
        System.out.println("INFO: the path value passed by col is: " + filePath + '\n');
        String urlAttach = settings.getString(Consts.APP_DATASTORAGE_URL);
        /**
        // if it contains "/datastorage" get rid of everything before
        if (filePath.contains(urlAttach)) {
          filePath = filePath.substring(filePath.lastIndexOf(urlAttach));
        }
        */         
        
        List<FileOrderResource> filepath_list = new ArrayList<FileOrderResource>();      
        if(sdometamodel.get_pk_sdo().contains("hmi")){
          filepath_list = getfilepath(filePath,urlAttach,sdometamodel.get_pk_sdo(), sdometamodel.getPre_path_hmi());
        }
        if(sdometamodel.get_pk_sdo().contains("aia")){
          filepath_list = getfilepath(filePath);
        }
             
        System.out.println("filepathlis is a null list?: " + filepath_list.size() + "\n");
        for(FileOrderResource file_resource : filepath_list){
          String f_path = file_resource.getFile_path_storage();   // get the stockage path for aia and hmi
          String org_Name = file_resource.getFile_name();         // get the original fits name 
           
          file_resource.setFile_name(sdometamodel.getNew_fits_name() + org_Name);
                                                                  // reset the fits name, add wave, date etc before the original fits name 
          System.out.println("INFO: the original fits name is : " +org_Name + ",  And the new name is : " + file_resource.getFile_name() + "\n");
                    
        if (f_path.contains(urlAttach)) {
          f_path = f_path.substring(filePath.lastIndexOf(urlAttach));
          }
        Reference ref = new Reference();
        if (f_path.startsWith("http://")) {
          ref.setPath(f_path);
          ref.setQuery(file_resource.getFile_name());
          list.addReferenceSource(ref);
        }
        else {
          ref.setPath(RIAPUtils.getRiapBase() + f_path);
          ref.setQuery(file_resource.getFile_name());
          list.addReferenceSource(ref);
        }     
        }
      }
    }
    return list;
  }

  
  
  /** Create a function to cut the series_name and the recnum in the file path after /datastorage/user */

  /**  create the function to get the series_name in sdo 
   * 
   *  @param series_name  : Identity key for AIA data
   *  @param path_data    : Create different storage path for hmi data and aia data
   *      
   *  */
  
  public SdoMetaModel getSdoModel(Record rec){
    
    /** Create a sdo model to save the value and information from dbRequest  */
    SdoMetaModel sdometamodel= new SdoMetaModel();
    List<AttributeValue> listAttributes = rec.getAttributeValues();
    //AttributeValue obj = null;
    String seriesname = null; 
    String sunum = null;
    String recnum = null;
    String wave=null;
    String date=null;
    String pk_sdo = null;
    String pre_hmi_path = null;
    String new_fits_name = null;
   
    System.out.println("listAttributes : "+listAttributes.size() + "rec is : " +rec.getId() +  rec.toString());
//    System.out.println("listAttributes No.1: "+ listAttributes.get(0).getName().toString()+listAttributes.get(1).getName().toString() + listAttributes.get(2).getValue().toString());

    for(int i=0; i<listAttributes.size(); i++)
    {
  //    System.out.println("listAttributes No: " + i+ listAttributes.get(i).getName().toString() + listAttributes.get(i).getValue().toString() );
      if (listAttributes.get(i).getName().equals("recnum")) {
        recnum = listAttributes.get(i).getValue().toString();
      // System.out.println("recnum : "+recnum);
      }     
      if (listAttributes.get(i).getName().equals("sunum")) {
        sunum = listAttributes.get(i).getValue().toString();
     //   System.out.println("INFO sunum : "+sunum);
      }
      if (listAttributes.get(i).getName().equals("series_name")) {
        seriesname = listAttributes.get(i).getValue().toString();
     //   System.out.println("series_name : "+seriesname);
      }
      if (listAttributes.get(i).getName().equals("date__obs")) {
        date = listAttributes.get(i).getValue().toString();
      //  System.out.println("date__obs : "+date);
      }

      if (listAttributes.get(i).getName().equals("wavelnth")) {
        wave = listAttributes.get(i).getValue().toString();
      //  System.out.println("wavelnth : "+wave);
      }    
    }
    if(seriesname.contains("hmi")){
      pk_sdo = recnum + seriesname;
      new_fits_name = seriesname + "." + wave + "." +recnum +"." + date +"Z.";
      pre_hmi_path = recnum+"/"+seriesname;
    }
    else if(seriesname.contains("aia")){
        pk_sdo = sunum + seriesname;
        new_fits_name = seriesname + "." + wave + "."+sunum +"."+date+"Z.";
        pre_hmi_path = null;
      }        
    
    sdometamodel.set_series_name(seriesname);
    sdometamodel.set_sunum(sunum);
    sdometamodel.set_recnum(recnum);
    sdometamodel.set_wave(wave);
    sdometamodel.set_date_obs(date);
    sdometamodel.set_pk_sdo(pk_sdo);
    sdometamodel.setPre_path_hmi(pre_hmi_path);
    sdometamodel.setNew_fits_name(new_fits_name);
    
    return sdometamodel;
    
  }
  
  
  /**  create the function to rename the original fits file 
   * 
   *  @param sunum  : Identity key for AIA data
   *  @param pk_hmi : Identity key for HMI data (1. recnum + series_name 2. column pk_monitor_slony in dataset )
   *    
   **/
  
  public List<FileOrderResource> getfilepath(String filepath){
    String filepath_temp = null;
    List<FileOrderResource> filepath_list = new ArrayList<FileOrderResource>();
    filepath_temp = filepath + "/image_lev1.fits";
      //get the string of path like http:// ***/SUM12/D7021542/50000 for AIA but 2055259/hmi.sharp_cea_720s_nrt/SUM12/D817152929/S00000 for HMI
    filepath_list.add(new FileOrderResource("image_lev1.fits", filepath_temp, "null"));
    System.out.println("file path: " + filepath_temp + '\n');
    return filepath_list;    

  }
  
  public List<FileOrderResource> getfilepath(String filepath, String urlAttach, String pk_sdo, String pre_hmi_stockage){
    String filepath_temp = null;
    // if it contains "/datastorage" get rid of everything before
    if(filepath.contains(urlAttach)){
      filepath_temp = filepath.substring(filepath.lastIndexOf(urlAttach)).split("/datastorage/user")[1];
    }   //get the string of path like SUM12/D7021542/50000 for AIA but 2055259/hmi.sharp_cea_720s_nrt/SUM12/D817152929/S00000 for HMI
    String path_stockage_hmi = null;
    if(pk_sdo.contains("hmi"))
    {
      path_stockage_hmi = filepath_temp.split(pre_hmi_stockage)[1];
      filepath_temp = path_stockage_hmi;
    }
    File dir = new File(filepath_temp);
    File[] fits_list = dir.listFiles();
    List<FileOrderResource> filepath_list = new ArrayList<FileOrderResource>();
    System.out.println("The directory/Path of files is " + filepath_temp +", please check your path" + "\n");
    if(fits_list != null){
      for(File file : fits_list){   
        if ((file.isFile()||file.getName().endsWith(".fits"))){
          String filename = file.getName();
          String file_path2 =  "http://localhost:8184/sitools/datastorage/user" +filepath_temp +"/" +filename;   // You need add datastorage to the url string for later riap call
          filepath_list.add(new FileOrderResource(filename, file_path2, pre_hmi_stockage));
        //  System.out.println("INFO : One file is added, its filename : " + filename + " and its stockage file path : " + file_path2 + "the pre_hmi_stockage "+ pre_hmi_stockage + "\n" );
        }
        else if(file.isDirectory()){
        //  System.out.println("INFO : the path - " + file.isDirectory() + " is not a file but a path, the system will enter this directory to find other fits files. ");
          getfilepath(file.getAbsolutePath(),urlAttach,pk_sdo, pre_hmi_stockage);
        }
      }
    }
    else{
       System.out.println("The director - " + path_stockage_hmi +" has no files" + "\n");
       }
    return filepath_list;    
  }
    
  
  @Override
  public Representation processOrder(ListReferencesAPI listReferences) throws SitoolsException {
    task.setCustomStatus("Order processing");
    OrderAPI.createEvent(order, getContext(), "PROCESSING ORDER");

    List<Reference> listOfFilesToOrder = listReferences.getReferencesSource();

    
    Reference destRef = OrderResourceUtils.getUserAvailableFolderPath(task.getUser(),
        settings.getString(Consts.USERSTORAGE_RESOURCE_ORDER_DIR) + folderName, getContext());

    ResourceModel resourceModel = getModel();
    ResourceParameter zipParam = resourceModel.getParameterByName("zip");

    // zip is a USER_INPUT parameter, let's get it from the request
    // parameters
    String zipValue = this.getRequest().getResourceRef().getQueryAsForm().getFirstValue("zip");
    if (zipValue == null || zipValue.equals("") || (!"false".equals(zipValue) && !"true".equals(zipValue))) {
      zipValue = zipParam.getValue();
    }

    Boolean zip = Boolean.parseBoolean(zipValue);
    if (zip) {
      task.getLogger().log(Level.INFO, zipParam.getName().toUpperCase() + " in progress for user : " 
          + task.getUser().getIdentifier() + " -> ip :" + getClientInfo().getUpstreamAddress());
      
      task.getLogger().info("List of files ordered :");
      for (Reference r : listReferences.getReferencesSource()) {
        task.getLogger().info(" - " + r.getIdentifier().substring(16));
        r.getPath();
      }
      zip(listOfFilesToOrder, listReferences, destRef);
    }
    else {
      task.getLogger().log(Level.INFO, "FILE in progress for user : "
          + task.getUser().getIdentifier() + " -> ip :" + getClientInfo().getUpstreamAddress());
      task.getLogger().info("List of files ordered :");
      for (Reference r : listReferences.getReferencesSource()) {
        task.getLogger().info(" - " + r.getIdentifier().substring(16));
        r.getPath();
      }
      
      Reference ref;
      for (Iterator<Reference> iterator = listOfFilesToOrder.iterator(); iterator.hasNext();) {
        Reference sourceRef = iterator.next();
        task.getLogger().log(Level.WARNING, "{0}", sourceRef);
        try {
          ref = new Reference(destRef);
          ref.addSegment(sourceRef.getLastSegment());
          OrderResourceUtils.copyFile(sourceRef, ref, getRequest().getClientInfo(), getContext());
          listReferences.addReferenceDest(ref);
        }
        catch (SitoolsException e) {
          task.getLogger().log(Level.WARNING, "File not copied : " + sourceRef, e);
        }
      }
    }

    task.getLogger().log(Level.INFO, "Number of downloaded files : " + listOfFilesToOrder.size());
    
    // set the result in the task
    task.setUrlResult(settings.getString(Consts.APP_URL) + settings.getString(Consts.APP_ORDERS_USER_URL) + "/"
        + order.getId());

    try {
      // copy the indexFile to the destination reference
      String orderFileListName = fileName;
      if (orderFileListName == null || "".equals(orderFileListName)) {
        orderFileListName = OrderResourceUtils.FILE_LIST_PATTERN.replace("{orderName}", ds.getName());
        orderFileListName = orderFileListName.replace("{timestamp}", formatedDate);
      }
      else {
        orderFileListName += "_fileList";
      }
      destRef.addSegment(orderFileListName);
      destRef.setExtensions("txt");
      Reference urlUserIndex = listReferences.copyToUserStorage(destRef, getContext(), getClientInfo());

      // add it the order
      ArrayList<String> orderedResource = new ArrayList<String>();
      orderedResource.add(urlUserIndex.toString());
      order.setResourceCollection(orderedResource);
      order = OrderAPI.updateOrder(order, getContext());

    }
    catch (IOException e) {
      throw new SitoolsException("Error while creating the file index in the userstorage", e);
    }
    return null;

  }

  /**
   * Create the Zip from the listOfFilesToOrder
   * 
   * @param listOfFilesToOrder
   *          the list of files to order
   * @param listReferences
   *          the ListReferenceAPI to add some reference
   * @param destRef
   *          the destination reference
   * @throws SitoolsException
   *           if there is an error
   */
  private void zip(List<Reference> listOfFilesToOrder, ListReferencesAPI listReferences, Reference destRef)
    throws SitoolsException {

    String zipFileName = fileName;
    if (zipFileName == null || "".equals(zipFileName)) {
      zipFileName = OrderResourceUtils.ZIP_FILE_PATTERN.replace("{orderName}", ds.getName());
      zipFileName = zipFileName.replace("{timestamp}", formatedDate);
    }

    Reference zipRef = new Reference(RIAPUtils.getRiapBase() + settings.getString(Consts.APP_TMP_FOLDER_URL));
    zipRef.addSegment(zipFileName);
    zipRef.setExtensions("zip");

    // create an index and add it to the zip files
    Reference ref;
    Reference sourceRef;
    for (Iterator<Reference> iterator = listOfFilesToOrder.iterator(); iterator.hasNext();) {
      sourceRef = iterator.next();
      ref = new Reference(destRef);
      ref.addSegment(sourceRef.getLastSegment());
      listReferences.addReferenceDest(ref);
    }

    // copy the indexFile to the destination reference
    Reference destRefListFileInZip = new Reference(destRef);

    String orderFileListName = fileName;
    if (orderFileListName == null || "".equals(orderFileListName)) {
      orderFileListName = OrderResourceUtils.FILE_LIST_PATTERN.replace("{orderName}", ds.getName());
      orderFileListName = orderFileListName.replace("{timestamp}", formatedDate);
    }
    else {
      orderFileListName += "_fileList";
    }
    destRefListFileInZip.addSegment(orderFileListName);
    destRefListFileInZip.setExtensions("txt");
    try {
      listReferences.copyToUserStorage(destRefListFileInZip, getContext(), getClientInfo());
      listReferences.clearReferencesDest();
      listReferences.addReferenceSource(destRefListFileInZip);
    }
    catch (IOException e) {
      e.printStackTrace();
    }

    OrderResourceUtils.zipFiles(listOfFilesToOrder, settings.getTmpFolderUrl() + "/" + zipFileName + ".zip",
        getRequest().getClientInfo(), getContext());
    destRef.addSegment(zipRef.getLastSegment());
    OrderResourceUtils.copyFile(zipRef, destRef, getRequest().getClientInfo(), getContext());
    OrderResourceUtils.deleteFile(zipRef, getRequest().getClientInfo(), getContext());

    Reference destZipRef = new Reference(destRef);
    listReferences.addReferenceDest(destZipRef);

    destRef.setLastSegment("");
  }
}

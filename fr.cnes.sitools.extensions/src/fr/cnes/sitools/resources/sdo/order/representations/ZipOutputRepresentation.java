/*******************************************************************************
 * Copyright 2010-2014 CNES - CENTRE NATIONAL d'ETUDES SPATIALES
 *
 * This file is part of SITools2.
 *
 * SITools2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SITools2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SITools2.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.cnes.sitools.resources.sdo.order.representations;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.restlet.Context;
import org.restlet.data.ClientInfo;
import org.restlet.data.Disposition;
import org.restlet.data.MediaType;
import org.restlet.data.Reference;
import org.restlet.representation.OutputRepresentation;
import org.restlet.representation.Representation;

import fr.cnes.sitools.common.exception.SitoolsException;
import fr.cnes.sitools.resources.order.utils.ListReferencesAPI;
import fr.cnes.sitools.resources.order.utils.OrderResourceUtils;
import fr.cnes.sitools.resources.sdo.order.FileOrderResource;

/**
 * Representation used to create a Zip Archive from a List of {@link Reference}
 * pointing to some files
 * 
 * 
 * @author m.gond
 */
public class ZipOutputRepresentation extends OutputRepresentation {
  /**
   * the list of {@link Reference}
   */
  private List<Reference> references;
  /** The clientInfo */
  private ClientInfo clientInfo;
  /** The Context */
  private Context context;
  /** The source reference map */
  private Map<Reference, String> refMap;
  
  private String ipClient;


  /**
   * Create a new {@link ZipOutputRepresentation}
   * 
   * @param referencesSource
   *          the list of {@link Reference} pointing the file to add to the zip
   *          archive
   * @param clientInfo
   *          the clientInfo of the current user
   * @param context
   *          the Context
   * @param fileName
   *          the complete fileName with extension
   * @param fit_map
   *          file name hashmap   
   */
  
  public ZipOutputRepresentation(List<Reference> referencesSource, ClientInfo clientInfo, Context context,
      String fileName) {
    super(MediaType.APPLICATION_ZIP);
    references = referencesSource;
    this.clientInfo = clientInfo;
    this.context = context;
    this.ipClient =clientInfo.getUpstreamAddress();  

    Disposition disp = new Disposition(Disposition.TYPE_ATTACHMENT);
    disp.setFilename(fileName);
    this.setDisposition(disp);
  }

  /**
   * 
   * @param refListAPI
   * @param clientInfo
   * @param context
   * @param fileName
   */
  public ZipOutputRepresentation(ListReferencesAPI refListAPI, ClientInfo clientInfo, Context context, String fileName) {
    super(MediaType.APPLICATION_ZIP);
    references = refListAPI.getReferencesSource();
    refMap = refListAPI.getRefSourceTarget();
    this.clientInfo = clientInfo;
    this.context = context;
    this.ipClient =clientInfo.getUpstreamAddress();  

    Disposition disp = new Disposition(Disposition.TYPE_ATTACHMENT);
    disp.setFilename(fileName);
    this.setDisposition(disp);
  }
  


  

  @Override
  public void write(OutputStream outputStream) throws IOException {

    String DatasetResLog ="Here is the log for SDODirectOrderResourceModel - Zip only, it is for the download plugin for datasets: \n";
    DatasetResLog += "   - SDO/AIA \n";
    DatasetResLog += "   - SDO/HIM \n";
    Date myDateStart = new Date();
    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy  HH'h'mm:ss");
    DateFormat dateFormat_log = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
    DatasetResLog +="Zip in progress for user : "+"guest"+" ip : "+ipClient+"\n"+references.size()+" files requested\n"+"Start : "+dateFormat.format(myDateStart)+"\n";
    context.getLogger().log(Level.INFO,DatasetResLog);
    DatasetResLog +="List of files added :\n";    
    try {
      // create a new ZipOutputStream

      ZipOutputStream zipOutput = new ZipOutputStream(outputStream);
      // loop through the References

      long totalArchiveSize = 0;
      int buffersize = 1024;
      byte[] buf = new byte[buffersize];
      for (Reference reference : references) {
        if (reference.getLastSegment() != null) {
          try {
            context.getLogger().log(Level.FINE, "WRITE : " + reference + " into zip file");
            // try to get the file
            Representation repr = OrderResourceUtils.getFile(reference, clientInfo, context);
            // create a new ZipEntry with the name of the entry

            InputStream stream = null;
           
            String f_path = reference.getPath().split("/datastorage/user")[1];
            ZipEntry zipEntry=null;
            zipEntry = new ZipEntry(reference.getQuery().toString());          
            DatasetResLog +="\t"+reference.getQuery().toString()+"\n";
            zipOutput.putNextEntry(zipEntry);
            System.out.println("The zipEntry path : "+f_path + " and the zipEntry name = " + zipEntry.getName() + ", zipEntry size : " + repr.getSize()/1048576.0+" MB\n");

            totalArchiveSize += zipEntry.getSize();
            try {
              // get the stream of the File, read it and write it to
              // the Zip stream
              stream = repr.getStream();
              int count = 0;
              while ((count = stream.read(buf, 0, buffersize)) != -1) {
                zipOutput.write(buf, 0, count);
              }

            }
            catch (Exception e) {
              context.getLogger().log(Level.INFO, "File " + reference + "canno't be found", e);
            }
            finally {
              // close the entry and the file stream
              zipOutput.closeEntry();
              if (stream != null) {
                stream.close();
              }
            }
          }
          catch (SitoolsException ex) {
            // if the file canno't be found, log the error and go to the
            // next file
            context.getLogger().log(Level.INFO, "File " + reference + "canno't be found", ex);
          }
        }
      }
      
      
      Date myDateEnd = new Date();
      DatasetResLog += "End : " + dateFormat.format(myDateEnd)+"\n";
      long interval =myDateEnd.getTime()-myDateStart.getTime();
      int millisec= (int) (interval % 1000);
      int seconds = (int) (interval / 1000) % 60 ;
      int minutes = (int) ((interval / (1000*60)) % 60);
      int hours   = (int) ((interval / (1000*60*60)) % 24);
      DatasetResLog += "Time taken  : "+hours+"h "+minutes+"min "+seconds+"sec "+millisec+"ms\n";
      DatasetResLog +="Nbr files downloaded in zip: " + references.size()+ "\nTotal downloaded : "+totalArchiveSize/1048576.0+" MB\n";
      
      String file_path = "../../data/resourses_logs_solar/"+ "SDODatasetDownloadZIPPlugin-" +dateFormat_log.format(myDateEnd)+ ".log";
      System.out.println("The log file is added: " + file_path);

      context.getLogger().log(Level.INFO, DatasetResLog);
      // Log the zip estimated size
      context.getLogger().info("Total size (estimated, uncompressed) : " + totalArchiveSize + "kB");
      // close the zip stream
      
      contextToFileLog(DatasetResLog,file_path);
      
      
      // Log the zip estimated size
      context.getLogger().info("Total size (estimated) : " + totalArchiveSize + "kB");
      // close the zip stream
      zipOutput.close();
    }
    catch (Exception e) {
      context.getLogger().log(Level.SEVERE, e.getMessage(), e);
    }
  }

  /** very stupid method to get the log, not a good code to get the code according to the log level**/
  public void contextToFileLog(String log, String file_name) {
         
      
      BufferedWriter writer = null;
      try
      {
          writer = new BufferedWriter(new FileWriter(file_name));
          writer.write(log);
          System.out.println("The log file is added: " + file_name);

      }
      catch ( IOException e)
      {
      }
      finally
      {
          try
          {
              if ( writer != null)
              writer.close( );
          }
          catch ( IOException e)
          {
          }
      }
   
}
  




}

     /*******************************************************************************
 * Copyright 2010-2016 CNES - CENTRE NATIONAL d'ETUDES SPATIALES
 *
 * This file is part of SITools2.
 *
 * SITools2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SITools2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SITools2.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.cnes.sitools.dataset.plugins.filters.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.restlet.Request;
import org.restlet.data.Form;
import org.restlet.ext.wadl.ParameterInfo;
import org.restlet.ext.wadl.ParameterStyle;

import fr.cnes.sitools.common.validator.ConstraintViolation;
import fr.cnes.sitools.common.validator.Validator;
import fr.cnes.sitools.dataset.DataSetApplication;
import fr.cnes.sitools.dataset.filter.business.AbstractFilter;
import fr.cnes.sitools.dataset.model.Column;
import fr.cnes.sitools.dataset.model.DataSet;
import fr.cnes.sitools.dataset.model.Operator;
import fr.cnes.sitools.dataset.model.Predicat;
import fr.cnes.sitools.util.SQLUtils;

/**
 * Filter defined for Multiple Value Component
 * 
 * 
 * @author d.arpin
 */
public final class MultipleWaveGroupFilter extends AbstractFormFilter {

  /**
   * The list of component that uses this filter
   */
  private enum TYPE_COMPONENT {
    /** List box type */
    WAVEGROUPMULTILIST,
    /** CheckBox */
    CHECKBOX
  }

  /**
   * Default constructor
   */
  public MultipleWaveGroupFilter() {

    super();
    this.setName("WAVEGROUPMULTILIST");
    this.setDescription("Required when using grouped WAVELENGTH Multiple Value Components");

    this.setClassAuthor("WANG GUANJI");
    this.setClassOwner("IAS");
    this.setClassVersion("0.1");
    this.setDefaultFilter(true);

    HashMap<String, ParameterInfo> rpd = new HashMap<String, ParameterInfo>();

    ParameterInfo paramInfo;
    paramInfo = new ParameterInfo("p[#]", false, "xs:string", ParameterStyle.QUERY,
        "LISTBOXMULTIPLE|columnAlias|value1|...|value n");
    rpd.put("0", paramInfo);
    paramInfo = new ParameterInfo("p[#]", false, "xs:string", ParameterStyle.QUERY,
        "CHECKBOX|columnAlias|value1|...|value n");
    rpd.put("1", paramInfo);

    paramInfo = new ParameterInfo("c[#]", false, "xs:string", ParameterStyle.QUERY,
        "LISTBOXMULTIPLE|dictionaryName,conceptName|value1|...|value n");
    rpd.put("2", paramInfo);
    paramInfo = new ParameterInfo("c[#]", false, "xs:string", ParameterStyle.QUERY,
        "CHECKBOX|dictionaryName,conceptName|value1|...|value n");
    rpd.put("3", paramInfo);
    this.setRequestParamsDescription(rpd);
    //

  }

  @Override
  public List<Predicat> createPredicats(Request request, List<Predicat> predicats) throws Exception {
    // Get the dataset
    DataSetApplication dsApplication = null;
    DataSet ds = null;
    boolean isConcept = true;
    Form params = request.getResourceRef().getQueryAsForm();
    boolean filterExists = true;
    int i = 0;
    // Build predicat for filters param
    while (filterExists) {
      // first check if the filter is applied on a Concept or not
      String index = TEMPLATE_PARAM_CONCEPT.replace("#", Integer.toString(i));
      String formParam = params.getFirstValue(index);
      if (formParam == null) {
        isConcept = false;
        index = TEMPLATE_PARAM.replace("#", Integer.toString(i));
        formParam = params.getFirstValue(index);
      }
      i++;
      if (formParam != null) {
        String[] parameters = formParam.split("\\|");
        TYPE_COMPONENT[] types = TYPE_COMPONENT.values();
        Boolean trouve = false;
        for (TYPE_COMPONENT typeCmp : types) {
          if (typeCmp.name().equals(parameters[TYPE])) {
            trouve = true;
          }
        }
        if (trouve) {
          if (dsApplication == null) {
            dsApplication = (DataSetApplication) getContext().getAttributes().get("DataSetApplication");
            ds = dsApplication.getDataSet();
          }
          String columnAlias = null;
          String order_wave = null;
          if (parameters.length >= VALUES) {

            /*
             * columnsAlias = parameters[COLUMN].split(","); ArrayList<Column> columns = new ArrayList<Column>(); for
             * (String columnAlias : columnsAlias) { Column col = ds.findByColumnAlias(columnAlias); if (col != null) {
             * columns.add(col); }             * 
             * }
             */     
            columnAlias = getColumnAlias(isConcept, parameters, dsApplication);
          //  System.out.println( " guanji columnAlias = "+ columnAlias.toString() + " the picard datasetID :" + ds.getId().toString() + "\n");
            /** columnAlias = lambda ; the picard datasetID :d5d2ea53-3373-4267-ac3c-9a47959da28b **/                    
            String picard_ds_ID = "d5d2ea53-3373-4267-ac3c-9a47959da28b";
            if (columnAlias != null) {
              Column col = ds.findByColumnAlias(columnAlias);

              if (col != null && col.getFilter() != null && col.getFilter()) {
                String[] values = Arrays.copyOfRange(parameters, VALUES, parameters.length);
                if (values != null) {
                  Predicat predicat = new Predicat();
                  predicat.setLeftAttribute(col);
                  predicat.setNbOpenedParanthesis(0);
                  predicat.setNbClosedParanthesis(0);
                  predicat.setCompareOperator(Operator.IN);

                  List<String> in = new ArrayList<String>();
                  
                  if(ds.getId().equals(picard_ds_ID)){
                    groupValueList(values, in);
                  }
                  else {
                  for (String value : values) {
                    // escape every value to avoid SQL injections
                    in.add(SQLUtils.escapeString(value));
                  }         
                  }               
                  predicat.setRightValue(in);
                  predicats.add(predicat);                   
                }
              }
            }

          }
        }
      }
      else {
        filterExists = false;
      }
    }

    return predicats;
  }
  
  public void groupValueList(String[] values, List<String> in){
    for (String value : values) {   
      if(value.equals("393"))
        in.add("393', '393.34', '393.4");
      if(value.equals("535"))
        in.add("535','535.7','535.75");
      if(value.equals("607"))
        in.add("607', '607.1', '607.23");
      if(value.equals("782"))
        in.add("782', '782.2', '782.37");     
    }
  }
  
  /**
   * Gets the validator for this Filter
   * 
   * @return the validator for the filter
   */
  @Override
  public Validator<AbstractFilter> getValidator() {
    return new Validator<AbstractFilter>() {
      @Override
      public Set<ConstraintViolation> validate(AbstractFilter item) {
        // TODO Auto-generated method stub
        return null;
      }
    };
  }

}

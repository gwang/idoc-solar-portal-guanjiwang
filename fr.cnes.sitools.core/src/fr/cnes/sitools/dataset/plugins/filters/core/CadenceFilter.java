    /*******************************************************************************
 * Copyright 2010-2014 CNES - CENTRE NATIONAL d'ETUDES SPATIALES
 *
 * This file is part of SITools2.
 *
 * SITools2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SITools2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SITools2.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package fr.cnes.sitools.dataset.plugins.filters.core;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.restlet.Request;
import org.restlet.data.Form;
import org.restlet.ext.wadl.ParameterInfo;
import org.restlet.ext.wadl.ParameterStyle;

import fr.cnes.sitools.common.validator.ConstraintViolation;
import fr.cnes.sitools.common.validator.Validator;
import fr.cnes.sitools.dataset.DataSetApplication;
import fr.cnes.sitools.dataset.filter.business.AbstractFilter;
import fr.cnes.sitools.dataset.model.Column;
import fr.cnes.sitools.dataset.model.DataSet;
import fr.cnes.sitools.dataset.model.Operator;
import fr.cnes.sitools.dataset.model.Predicat;
import fr.cnes.sitools.util.SQLUtils;

/**
 * Filter defined for Single Value Component
 * 
 * 
 * @author WANG GUANJI
 */
public final class CadenceFilter extends AbstractFormFilter {

  /**
   * The list of component that uses this filter
   */
  private enum TYPE_COMPONENT {

    /** Cadence Checkbox type */
    CADENCE
  }

  /**
   * Default constructor
   */
  public CadenceFilter() {

    super();
    this.setName("CadenceFilter");
    this.setDescription("Required when using time interval function in sdo database ");

    this.setClassAuthor("WANG GUANJI");
    this.setClassAuthor("WANG GUANJI");
    this.setClassOwner("IAS");
    this.setClassVersion("0.1");
    this.setDefaultFilter(true);

    HashMap<String, ParameterInfo> rpd = new HashMap<String, ParameterInfo>();

    ParameterInfo paramInfo;
    paramInfo = new ParameterInfo("p[#]", false, "xs:string", ParameterStyle.QUERY, "CADENCE|columnAlias|value");
    rpd.put("10", paramInfo);

    paramInfo = new ParameterInfo("c[#]", false, "xs:string", ParameterStyle.QUERY,
    "CADENCE|dictionaryName,conceptName|value");
    rpd.put("11", paramInfo);
    this.setRequestParamsDescription(rpd);
    //

  }

  public Map<String, String> getMapCadence(Map<String, String> map){
 //   map.put("none", "000000000");
    map.put("12 sec", "000000000");
    map.put("1 min",  "000000001");
    map.put("2 min",  "000000011");
    map.put("10 min", "000000111");
    map.put("30 min", "000001111");    
    map.put("1 h",    "000011111");
    map.put("2 h",    "000111111");
    map.put("6 h",    "001111111");
    map.put("12 h",   "011111111");    
    map.put("1 day",  "111111111");
    return map;
  }
  
  public Map<String, String> getMapCadence_hmi(Map<String, String> map){
    //   map.put("none", "000000000");
       map.put("12 min","000000001");    
       map.put("1 h",   "000000011");
       map.put("2 h",   "000000111");
       map.put("6 h",   "000001111");
       map.put("12 h",  "000011111");       
       map.put("1 day", "000111111");
       return map;
     }
  
  public String getMaskCadence(String value, Map<String, String> map)
  {
    String id = "";
    id = map.get(value);
    return id;
  }
  
  @Override
  public List<Predicat> createPredicats(Request request, List<Predicat> predicats) throws Exception {
    DataSetApplication dsApplication = null;
    DataSet ds = null;
    boolean isConcept = true;
    Form params = request.getResourceRef().getQueryAsForm();
    boolean filterExists = true;
    int i = 0;
    // Build predicat for filters param
    while (filterExists) {
      // first check if the filter is applied on a Concept or not
      String index = TEMPLATE_PARAM_CONCEPT.replace("#", Integer.toString(i));
      String formParam = params.getFirstValue(index);
      if (formParam == null) {
        isConcept = false;
        index = TEMPLATE_PARAM.replace("#", Integer.toString(i));
        formParam = params.getFirstValue(index);
      }
      i++;
      if (formParam != null) {
        String[] parameters = formParam.split("\\|");
        TYPE_COMPONENT[] types = TYPE_COMPONENT.values();
        Boolean trouve = false;
        for (TYPE_COMPONENT typeCmp : types) {
          if (typeCmp.name().equals(parameters[TYPE])) {
            trouve = true;
          }
        }
        if (trouve) {
          if (dsApplication == null) {
            dsApplication = (DataSetApplication) getContext().getAttributes().get("DataSetApplication");
            ds = dsApplication.getDataSet();
          }
          String columnAlias = null;
          if (parameters.length >= VALUES) {

            columnAlias = getColumnAlias(isConcept, parameters, dsApplication);
            if (columnAlias != null) {
              Column col = ds.findByColumnAlias(columnAlias);
           //   System.out.println("col equals : " + col.getColumnAlias() + ' ' + col.toString()); //col equals : mask_cadence
              /**                                    @mask_cadence
               *                                     
               * fr.cnes.sitools.dataset.model.Column@38e5e12e for hmi
                 fr.cnes.sitools.dataset.model.Column@8b8c6538 for aia
                 
                 System.out.println("col equals : " + col.getTableAlias() + ' ' + col.getTableName() + ' ' + col.getDataIndex() );
                                                       @monitor_slony               @mask_cadence              @mask_cadence
               */

              if (col != null && col.getFilter() != null && col.getFilter()) {
                // get the value and escape it to avoid SQL injection
                String value = SQLUtils.escapeString(parameters[VALUES]);
                Map<String, String> map_cadence = new HashMap<String, String>();
               
                //Seperate the two types of data series name              
                if(col.toString().contains("38e5e12e")){
                  map_cadence = getMapCadence_hmi(map_cadence);
                }
                if(col.toString().contains("663c8d74a5a8")){
                  map_cadence = getMapCadence_hmi(map_cadence);
                }
                else if(col.toString().contains("8b8c6538")){                 
                  map_cadence = getMapCadence(map_cadence);
                }                
                String id_cadence = getMaskCadence(value, map_cadence);
                Predicat predicat = new Predicat();
                if (value != null) {
                  predicat.setLeftAttribute(col);
                  predicat.setNbOpenedParanthesis(0);
                  predicat.setNbClosedParanthesis(0);
                  predicat.setCompareOperator(Operator.GTE);
                  /*
                  if (parameters[TYPE].equals(TYPE_COMPONENT.CADENCE.name())) {                  
                    predicat.setCompareOperator(Operator.GTE);
                  }
*/
                  predicat.setRightValue("'" + id_cadence + "'");

                  predicats.add(predicat);
                }
              }
            }
          }
        }
      }

      else {
        filterExists = false;
      }
    }

    return predicats;
  }

  /**
   * Gets the validator for this Filter
   * 
   * @return the validator for the filter
   */
  @Override
  public Validator<AbstractFilter> getValidator() {
    return new Validator<AbstractFilter>() {
      @Override
      public Set<ConstraintViolation> validate(AbstractFilter item) {
        // TODO Auto-generated method stub
        return null;
      }
    };
  }

}
Ext.namespace('sitools.user.modules');

Ext.define( 'sitools.user.modules.AccordionChart', {
    extend: 'sitools.user.core.Module',


    init : function () {
/*
    	var view = Ext.create('sitools.user.view.modules.ChartExt.AccordionView');
        this.setViewCmp(view);

        this.show(this.getViewCmp());

        this.callParent(arguments);
*/
    
    
    	// Save the preference in admin role 
        var project = Ext.getStore('ProjectStore').getProject();
        Ext.Ajax.request({
            method : "GET",
            url : project.get('sitoolsAttachementForUsers'), 
            success : function (response) {
                var json = Ext.decode(response.responseText);                
                this.view = Ext.create('sitools.user.view.modules.ChartExt.AccordionView');              
                
                this.show(this.view);
            }, 
            failure : alertFailure, 
            scope : this
        });
        
    },
    
    /**
     * method called when trying to save preference
     * 
     * @returns
     */
    _getSettings : function () {
        return {
            preferencesPath : "/modules",
            preferencesFileName : this.id
        };

    }

} );

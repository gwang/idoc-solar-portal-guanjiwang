
/***************************************
* Copyright 2011 CNES - CENTRE NATIONAL d'ETUDES SPATIALES
* 
* This file is part of SITools2.
* 
* SITools2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* SITools2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with SITools2.  If not, see <http://www.gnu.org/licenses/>.
***************************************/
/*global Ext, sitools, i18n*/

Ext.namespace('sitools.user.modules');

/**
 * @param HTMLPanel
 * @author WANG GUANJI
 * Extension code for insert a HTML file into Sencha framework       
 */ 

Ext.define('sitools.user.modules.SOLARORBITERDescription', {
	extend : 'sitools.user.core.Module',	
    	/**
    	 * By extending the regular ol' Ext.Panel i've added the ability to load in html content from an external file:
    	 */
	 controllers : ['sitools.user.controller.modules.missionDescription.SOLARORBITERController'],
	 
    init : function () {
        
    	// Save the preference in admin role 
        var project = Ext.getStore('ProjectStore').getProject();
        Ext.Ajax.request({
            method : "GET",
            url : project.get('sitoolsAttachementForUsers'), 
            success : function (response) {
                var json = Ext.decode(response.responseText);
                
                this.view = Ext.create('sitools.user.view.modules.missionDescription.SOLARORBITERView', {
                	 header : false,
                     url    : "/sitools/common/html/solar_mission_description/SOLARORBITERModule.html",
                     bodyStyle: "background-position: center;background-repeat: no-repeat;background-color: black;background-image:url('/sitools/common/html/image_mission_description/ESA_logo.jpg') !important",
                     autoScroll : true
                });
                
                this.show(this.view);
            }, 
            failure : alertFailure, 
            scope : this
        });
        
    },
     /**
     * method called when trying to save preference
     * 
     * @returns
     */
    _getSettings : function () {
        return {
            preferencesPath : "/modules",
            preferencesFileName : this.id
        };

    }

});

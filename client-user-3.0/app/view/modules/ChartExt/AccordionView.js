/*******************************************************************************
 * Copyright 2010-2014 CNES - CENTRE NATIONAL d'ETUDES SPATIALES
 * 
 * This file is part of SITools2.
 * 
 * SITools2 is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * SITools2 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * SITools2. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/

/*global Ext, sitools, i18n, projectGlobal, alertFailure, showResponse*/

Ext.namespace('sitools.user.view.modules.ChartExt');
/**
 * Ext Chart Module
 * @class sitools.user.modules.AccordionChart
 * @extends Ext.Panel
 */
Ext.define('sitools.user.view.modules.ChartExt.AccordionView', {
    extend: 'Ext.panel.Panel',
    
    requires: [
        'Ext.layout.container.Accordion',
        'Ext.grid.*',
        'sitools.user.model.AccordionDataModel'
    ],
    xtype: 'layout-accordion',
    nbrecord_test : null,
    
    layout: 'accordion',
    width: 500,
    height: 400,
    defaults: {
        bodyPadding: 10
    },
    
    initComponent: function() {
    
    //	var nbrecord_test = null;
     this.accordionStore = Ext.create('sitools.user.store.AccordionStore');   
	 var project = Ext.getStore('ProjectStore').getProject();

     Ext.Ajax.request({
         url : project.get('sitoolsAttachementForUsers') + '/datasets',
         method : 'GET',
         scope : this,
         success : function (response) {
             var datasets = Ext.decode(response.responseText).data;
             var length = 0;
             for (var dataset in datasets){
            	// alert(dataset.id + ' '+dataset.name );
            	// alert(dataset.data.nbRecords); 
            	 ++length;
             }
    //         alert("the legnth is : " + length);
             
             /*
             Ext.each(datasets, function(dataset){
            	 alert(dataset.id + ' '+dataset.name );
            	 alert(dataset);
            	 alert(dataset.data.nbRecords);
           	 if(dataset.id == '754aaf5c-8a6e-4279-9296-c8d5847f88d5')
          //   if(dataset.name == 'AIA')
            		 {
            		 this.nbrecord_test = dataset.data.nbRecords;
            	//	 alert(dataset.name + dataset.id);
            	//	 alert(dataset.data.nbRecords);

            		 }
             },this);
             
             
         
             Ext.each(datasets, function(dataset){
                 dataset.text = dataset.name;
                 this.getRootNode().appendChild(dataset);
             }, this);
             */
         }
     });
     
     
     Ext.apply(this, {	
            items: [                    
              {
                 title: '<span style="font-color: black;">HOME</span>',
  	             html   : '<iframe src="/sitools/common/html/Solar-Module-html/homesolarModule.html" style="position: relative;  height: 100%; width: 100%;"></iframe>',
              },{            	
                bodyPadding: 0,
                xtype: 'grid',
                title: 'Missions data form',
              //  hideCollapseTool: true,
                columnLines: true,
                viewConfig: {
                    stripeRows: true
                },
                store: this.accordionStore,

                    columns: [{
                        text     : 'No.',
                        width     : 75,
                        height    : 60,
                        sortable : true,
                        dataIndex: 'num'
                    },{
                        text     : 'Mission',
                        width    : 150,
                        height    : 60,
                        sortable : false,
                        dataIndex: 'name'
                    }, {
                        text     : 'Date_begin',
                        width    : 150,
                        height    : 60,
                        sortable : true,
                        dataIndex: 'date_begin'
                    }, {
                        text     : 'Date_end',
                        width    : 150,
                        height    : 60,
                        sortable : false,
                        renderer : this.changeDateRenderer,
                        dataIndex: 'date_end'
                    },{
                        text     : 'NB_Records',
                        width    : 90,
                        height    : 60,
                        sortable : false,
                        renderer : this.changeNBRenderer,
                        dataIndex: 'nbrecord'
                    },{
                        text     : 'Image',
                        width    : 150,
                        height    : 60,
                        sortable : false,
                        renderer : this.changeImgRenderer,
                        dataIndex: 'img_mission'
                    }]
                },{
                title: '<span style="font-color: black;">SOHO Description</span>',
            	html    : '<iframe src="/sitools/common/html/solar_mission_description/SOHOModule.html" style="position: relative;  height: 100%; width: 100%;"></iframe>',
            }, {
                title: 'STEREO Description',
                html    : '<iframe src="/sitools/common/html/solar_mission_description/STEREOModule.html" style="position: relative;  height: 100%; width: 100%;"></iframe>',
            }, {
            	 title: 'PICARD Description',
                 html    : '<iframe src="/sitools/common/html/solar_mission_description/PICARDModule.html" style="position: relative;  height: 100%; width: 100%;"></iframe>',
             }, {
            	 title: 'CORONAS-F Description',
                 html    : '<iframe src="/sitools/common/html/solar_mission_description/CORONASModule.html" style="position: relative;  height: 100%; width: 100%;background-position: center;background-repeat: no-repeat; background-image:url(/sitools/common/html/image_mission_description/russia.png)!important;"></iframe>',
             }, {
                title: 'TRACE Description',
                html    : '<iframe src="/sitools/common/html/solar_mission_description/TRACEModule.html" style="position: relative;  height: 100%; width: 100%;"></iframe>',
     
             }, {
                title: 'SDO Description',
                html    : '<iframe src="/sitools/common/html/solar_mission_description/SDOModule.html" style="position: relative;  height: 100%; width: 100%;"></iframe>',
            }
            , {
                title: 'SOLAR ORBITER Description',
                html    : '<iframe src="/sitools/common/html/solar_mission_description/SOLARORBITERModule.html" style="position: relative;  height: 100%; width: 100%;"></iframe>',
                bodyStyle: "background-size:100% 100%; !background-color: transparent;background-repeat: no-repeat;"

            }]
        });
        this.callParent();
    },

    changeDateRenderer: function(date) {
    	var cur_date = new Date();
        if (date == '1900-01-01') {
        	date = Ext.Date.format(cur_date,'Y-m-d H:i:s')
            return '<span style="color:green;text-align : center;">' + date + '</span>';
        } else  {
            return '<span style="color:blue; text-align : center;">' + date + '</span>';
        }
        return  date;
    },
    
    changeImgRenderer: function(value) {
    	if(value == 'SOHO')
    		return '<img src = "/sitools/upload/soho.png" style="width :30%; height: 30%;  display: block;margin: 0 auto;"/>';
    	if(value == 'STEREO')
    		return '<img src = "/sitools/upload/Anniversary-NASA-STEREO-Satellites-Turn-5-2.jpg" style="width :30%; height: 50%;  display: block;margin: 0 auto;"/>';
    	if(value == 'PICARD')
    		return '<img src = "/sitools/upload/satellite_picard.jpg" style="width :30%; height: 30%;  display: block;margin: 0 auto;"/>';
    	if(value == 'CORONAS-F')
    		return '<img src = "/sitools/upload/coronas-f.jpg" style="width :30%; height: 30%;  display: block;margin: 0 auto;"/>';
    	if(value == 'TRACE')
    		return '<img src = "/sitools/upload/506998main_TRACE_artist_rendition.jpeg" style="width :30%; height: 30%;  display: block;margin: 0 auto;"/>';
    	if(value == 'AIA')
    		return '<img src = "/sitools/upload/aiaimage.jpg" style="width :30%; height: 30%;  display: block;margin: 0 auto;"/>';
    	if(value == 'HMI')
    		return '<img src = "/sitools/upload/hmi.gif" style="width :30%; height: 30%;  display: block;margin: 0 auto;"/>';
    	if(value == 'GAIA-DEM')
    		return '<img src = "/sitools/upload/gaia-dem.png" style="width :30%; height: 30%;  display: block;margin: 0 auto;"/>';
    	if(value == 'EUV-SYN')
    		return '<img src = "/sitools/upload/eit-syn.jpg" style="width :30%; height: 30%;  display: block;margin: 0 auto;"/>';
    },
    
    changeNBRenderer: function(nbrecord) {
    /*
    	var project = Ext.getStore('ProjectStore').getProject();
    	if (nbrecord == '132021809')
    		nbrecord = project.get('sitoolsAttachementForUsers');
    	
    	if (nbrecord == '2346349'){
    		//if(project.get('fr.cnes.sitools.util.Property').get(name)){
    	//		nbrecord = project.get('fr.cnes.sitools.util.Property').get(name);    
    		//if( project.get('dataSets').get('resource').get('id')  == '754aaf5c-8a6e-4279-9296-c8d5847f88d5')
    			nbrecord = this.nbrecord_test;
    	//		 console.log("Enter the renderer if sentences and the nbrecords equals : " +nbrecord + "\n" );
    			 alert("enter 2346349 alert and the nbrecord is " + nbrecord);
    		}
    	return nbrecord;
    	*/
        return '<span style="text-align : center;">' + nbrecord + '</span>';
    },

     _getSettings : function () {
         return {
            preferencesPath : "/modules",
            preferencesFileName : this.id,
            xtype : this.$className
        };
        }

})
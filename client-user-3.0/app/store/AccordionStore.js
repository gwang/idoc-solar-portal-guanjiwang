/*******************************************************************************
 * Copyright 2010-2014 CNES - CENTRE NATIONAL d'ETUDES SPATIALES
 * 
 * This file is part of SITools2.
 * 
 * SITools2 is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * SITools2 is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * SITools2. If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
/*global Ext, sitools, window */

Ext.define('sitools.user.store.AccordionStore', {
	    extend: 'Ext.data.ArrayStore',
	    alias: 'store.accordion1',
	    model: 'sitools.user.model.AccordiondataModel',
	    
	    proxy: {
	        type: 'ajax',
	        reader: {
	            type: 'json',
	            root: "data"
	        }
	    },
	    /*
	    listeners: {
	        scope: this,
	        load: function (store, records, options) {
	            Ext.each(records, function (record) {
	                if (record.get("status") === "ACTIVE") {
	                    var properties = record.get("properties");
	                    var img = null;
	                    var nbRecords;
	                    Ext.each(properties, function (property) {
	                        if (property.name === "imageUrl") {
	                            img = property.value;
	                        }
	                        if (property.name === "nbRecord") {
	                            record.set("nbRecords", parseInt(property.value));
	                        }
	                    });
	                    if (!Ext.isEmpty(img)) {
	                        record.set("image", img);
	                    } else {
	                        record.set("image", SITOOLS_DEFAULT_PROJECT_IMAGE_URL);
	                    }
	                } else {
	                    store.remove(record);
	                }
	            });
	            store.clearFilter();
	            store.sort();
	        }
	    },
	    */
	    data: [
	              [1,  'SOHO',     '1995-12-02 00:00:00', '2014-10-24 13:59:47', '2806577',   'SOHO'  ],
	              [2,  'STEREO',   '2006-11-07 14:54:40', '2015-12-31 00:05:00', '10872519',  'STEREO'],
	              [3,  'TRACE',    '1999-01-24 00:25:31', '2010-06-21 23:52:36', '614015',    'TRACE' ],
	              [4,  'CORONAS-F','2001-08-16 13:32:37', '2005-10-02 00:29:18', '80110',     'CORONAS-F'],
	              [5,  'AIA',      '1995-12-02 00:00:00', '1900-01-01',          '132021809', 'AIA'],
	              [6,  'HMI',      '1995-12-02 00:00:00', '1900-01-01',          '2346349',   'HMI'],
	              [7,  'PICARD',   '2009-12-23 15:31:00', '2015-02-11 14:39:00', '5492064',   'PICARD'],
	              [8,  'GAIA-DEM', '2010-05-13 00:04:35', '2016-07-23 23:35:05', '68107',     'GAIA-DEM'],
	              [9,  'EUV-SYN',  '1996-01-03 00:00:00', '2010-07-30 00:00:00', '18482',     'EUV-SYN']
	          ]
}

);























